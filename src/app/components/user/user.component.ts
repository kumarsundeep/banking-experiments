import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name:string;
  users: IUsers[];
  transaction: ITransaction;
  totalTransaction:number;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    console.log("init runs");
    this.name = "John Doe";

    this.transaction = {
      firstT: 40,
      lastT:100
    };
    this.totalTransaction = 140;

    //Users
    this.dataService.getUsers().subscribe((users) => {
      console.log(users);
      this.users = users;
    } );

  }



}

interface IUsers{
  name: string;
  email: string;
}
export interface ITransaction{
  firstT:number,
  lastT:number
}
