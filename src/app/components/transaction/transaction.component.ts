import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  currentTotal:number;

  constructor(private dataService: DataService) { console.log("transaction cons init")}

  ngOnInit() {
    console.log("transaction init");
    this.currentTotal=0;
  }
  addToTotalTrans(totalTrans){
    this.currentTotal = Number(totalTrans) + Number(this.currentTotal);
    console.log(this.currentTotal);
    this.dataService.lastCredit = Number(totalTrans);
    this.dataService.total = this.currentTotal;
    return this.dataService;
  }
  deleteToTotalTrans(deltotalTrans){

    this.currentTotal =   Number(this.currentTotal) - Number(deltotalTrans);
    //console.log(this.currentTotal);
     this.dataService.lastDebit = Number(deltotalTrans);
     this.dataService.total = this.currentTotal;
    console.log(this.dataService.total);
    return this.dataService.total;

  }

}
