import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
  total: number = 0;
  lastCredit: number = 0;
  lastDebit: number = 0;
  constructor(public http: Http) {
    console.log("data service connected...");


  }
  getUsers(){
    return this.http.get('https://jsonplaceholder.typicode.com/users').map(res => res.json());
  }


}
